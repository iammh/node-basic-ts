import express from 'express';
import bodyParser from 'body-parser';
import { Constants } from './config/Constants';
import { AuhtRouter } from './auth';
import { UserRouter } from './users';
import { isAuthenticated } from './middlewares';
import {Logger } from "./config/Logger";
import { WarehouseRouter } from './warehouses';

const App = express();

/**
 * Register Common Middlewares
 */
App.use(bodyParser.json());

/**
 * Rester Routers
 */
App.use(Constants.API_BASE+ "/auth", AuhtRouter)
App.use(Constants.API_BASE+ "/users", isAuthenticated, UserRouter)
App.use(Constants.API_BASE+ "/warehouses", isAuthenticated, WarehouseRouter)
global["logger"] = Logger

export default App