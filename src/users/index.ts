import { Router, Request, Response } from "express";

export const UserRouter = Router();

UserRouter.get("/", (request: Request, response: Response)=> {
    response.json("Users");
});