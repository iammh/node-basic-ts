import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from "typeorm";

export class Location {
   
    @Column({name: "address1", nullable: true})
    address1: string;

    @Column({name: "address2", nullable: true})
    address2: string;

    @Column({name: "city", nullable: true})
    city: string;

    @Column({name: "state", nullable: true})
    state: string;

    @Column({name: "country", nullable: true})
    country: string;
}

@Entity({name: "warehouses"})
export class Warehouse extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({name: "name", unique: true})
    name: string;

    @Column(type => Location)
    location: Location;
}