import { Router, Request, Response } from "express";
import { Warehouse } from "./entities/Warehouse";

export const WarehouseRouter = Router();
WarehouseRouter.get('/', async (request: Request, response:Response)=> {
    const output = {
        message: null,
        data: []
    };
    try {
        let warehouses = await Warehouse.find({});
        output.data = warehouses;
        output.message = "Warehouses fetched from database"
    } catch (error) {
        output.message = error.message;
    }
    response.json(output);
});

WarehouseRouter.post('/', async (request: Request, response:Response)=> {
    let body = request.body;
    const output = {
        message: null,
        data: []
    };
    let status = 200;
    try {
        let warehouse = new Warehouse();
        warehouse.name = body.name;
        warehouse.location = body.location;
        await warehouse.save(); 
        output.data.push(warehouse);
        output.message = "Successfully created warehouse";
        status = 202; 
    } catch (error) {
        output.message = error.message;
        status = 400;
    }

    response.status(status).json(output);
});

WarehouseRouter.put('/', (request: Request, response:Response)=> {
    response.json([]);
});

WarehouseRouter.delete('/', (request: Request, response:Response)=> {
    response.json([]);
});