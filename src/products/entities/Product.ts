import { BaseEntity, Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({name: "products"})
export class Product extends BaseEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({length: 200})
    name: string;

    @Column({})
    serial: string;

    
}